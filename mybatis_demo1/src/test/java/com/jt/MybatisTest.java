package com.jt;

import com.jt.mapper.UserMapper;
import com.jt.pojo.User;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.*;

@SpringBootTest
public class MybatisTest {

    @Autowired
    private UserMapper userMapper;

    //查询所用用户
    @Test
    public void getAllUser() {
        List<User> userList = userMapper.findAll();
        for (User user : userList) {
            System.out.println(user);
        }
    }

    //查询单个用户
    @Test
    public void getUser() {
        int id = 7;
        User user = userMapper.findById(id);
        System.out.println(user);
    }

    //新增用户
    @Test
    public void setUser() {
        User user = new User();
        user.setId(100).setName("元宵节").setAge(100).setSex("女");
        Integer result = userMapper.saveUser(user);
        System.out.println("ok" + result);
    }

    //修改
    @Test
    public void updateUser() {
        User user = new User();
        user.setId(230).setName("清明节");
        Integer result = userMapper.updateUser(user);
        System.out.println(result + ": ok");
    }

    //删除
    @Test
    public void deleteUser() {
        Integer result = userMapper.deleteUser("清明节");
        System.out.println(result);
    }

    //查询年龄在18到100之间的所有用户
    @Test
    public void selectUserByMap() {
        HashMap<String, Integer> map = new HashMap<>();
        map.put("minAge", 18);
        map.put("maxAge", 100);
        List<User> allUserByMap = userMapper.findAllUserByMap(map);
        System.out.println(allUserByMap.toString());
    }

    //查询id在13~25之间的用户
    @Test
    public void getFindUserByMid() {
        int minId = 13;
        int maxId = 25;
        List<User> users = userMapper.findUserByMid(minId, maxId);
        for (User user : users) {
            System.out.println(user);
        }
    }

    //模糊查询
    @Test
    public void findUserByLike() {
        String name = "君";
        List<User> list = userMapper.findUserByLike(name);
        System.out.println(list);
    }

    //in 数组
    @Test
    public void getFindUserByInArray() {
        Integer[] array = {1, 3, 4, 5, 6};
        List<User> users = userMapper.findUserByInArray(array);
        for (User user : users) {
            System.out.println(user);
        }
    }

    //list集合
    @Test
    public void getFindUserByInList() {
        Integer[] array = {1, 3, 4, 5, 6};
        //将数组添加到集合中
        List<Integer> list = Arrays.asList(array);
        List<User> users = userMapper.findUserByInList(list);
        for (User user : users) {
            System.out.println(user);
        }
    }

    //map集合
    @Test
    public void getFindUserByInMap() {
        Integer[] array = {1, 3, 4, 5, 6};
        //将数组添加到集合中
        HashMap<String, Integer[]> map = new HashMap<>();
        map.put("array", array);
        List<User> users = userMapper.findUserByInMap(map);
        for (User user : users) {
            System.out.println(user);
        }
    }
}
