package com.jt;

import com.jt.mapper.UserMapper;
import com.jt.mapper.UserMapper2;
import com.jt.pojo.User;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

@SpringBootTest
public class MybatisTest2 {

    @Autowired
    private UserMapper2 userMapper2;

    //根据对象中不为空的属性，查询数据
    @Test
    public void getUser() {
        User user = new User();
        user.setName("黑熊精").setSex("男");
        List<User> user1 = userMapper2.findUser(user);
        System.out.println(user1);
    }

    //跟新用户数据
    @Test
    public void updateUser() {
        User user = new User();
        user.setId(5).setName("御弟哥哥").setAge(18);
        Integer result = userMapper2.updateUser(user);
        System.out.println("ok : " + result);
    }

    //分支结构查询
    @Test
    public void findUserByChoose() {
        User user = new User();
//        user.setName("御弟哥哥").setAge(18).setSex("男");
        user.setAge(18).setSex("男");
        List<User> users = userMapper2.findUserByChoose(user);
        for (User user1 : users) {
            System.out.println(user1);
        }
    }
}
