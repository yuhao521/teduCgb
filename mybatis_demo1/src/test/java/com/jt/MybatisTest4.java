package com.jt;

import com.jt.mapper.DeptMapper;
import com.jt.mapper.EmpMapper;
import com.jt.pojo.Dept;
import com.jt.pojo.Emp;
import org.apache.ibatis.session.SqlSession;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@SpringBootTest
public class MybatisTest4 {

    @Autowired
    private EmpMapper empMapper;

    @Autowired
    private DeptMapper deptMapper;

    //查询部门的全部信息
    @Test
    public void findAllDept() {
        List<Dept> depts = deptMapper.findAllDept();
        for (Dept dept : depts) {
            System.out.println(dept);
        }
    }

    //查询所有的员工信息
    @Test
    public void findAllEmp() {
        List<Emp> emps = empMapper.findAllEmp();
        for (Emp emp : emps) {
            System.out.println(emp);
        }
    }
}
