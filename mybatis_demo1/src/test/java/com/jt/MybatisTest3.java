package com.jt;

import com.jt.mapper.UserMapper2;
import com.jt.mapper.UserMapper3;
import com.jt.pojo.User;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
public class MybatisTest3 {

    @Autowired
    private UserMapper3 userMapper3;

    //别名查询
    @Test
    public void findAll(){
        List<User> users = userMapper3.findAll();
        for (User user : users) {
            System.out.println(user);
        }
    }
}
