package com.jt.mapper;

import com.jt.pojo.User;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Mapper //将接口交给Spring容器管理 Map<userMapper,JDK代理对象>
//@Repository //将持久层交给Spring容器管理
public interface UserMapper {
    //查询所有的用户信息
    List<User> findAll();

    //查询单个用户
    User findById(Integer id);

    //新增用户
    Integer saveUser(User user);

    //修改用户
    Integer updateUser(User user);

    //删除用户
    Integer deleteUser(String name);

    //查询年龄在18到100之间的所有用户
    List<User> findAllUserByMap(Map<String, Integer> map);

    //查询id在13~25之间的用户
    List<User> findUserByMid(@Param("minId") int minId, @Param("maxId") int maxId);

    //模糊查询
    List<User> findUserByLike(String name);

    //in查询
    //数组
    List<User> findUserByInArray(Integer[] array);

    //集合list
    List<User> findUserByInList(List<Integer> list);

    //集合map
    List<User> findUserByInMap(Map<String, Integer[]> map);
}
