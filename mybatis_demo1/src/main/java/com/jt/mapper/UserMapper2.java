package com.jt.mapper;

import com.jt.pojo.User;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface UserMapper2 {

    //查询所有用户
    List<User> findUser(User user);

    //更新数据
    Integer updateUser(User user);

    //根据选择查询用户
    List<User> findUserByChoose(User user);
}
