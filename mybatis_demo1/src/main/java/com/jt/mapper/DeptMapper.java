package com.jt.mapper;

import com.jt.pojo.Dept;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface DeptMapper {
    //查询部门中所有信息
    List<Dept> findAllDept();
}
