package com.jt.mapper;

import com.jt.pojo.Dept;
import com.jt.pojo.Emp;
import org.apache.ibatis.annotations.CacheNamespace;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
@CacheNamespace
public interface EmpMapper {

    //查询所有的员工信息
    List<Emp> findAllEmp();

    //查询所有部门信息
    List<Dept> findAllDept();

    //根据Id查询员工信息
    @Select("select * from emp where id = #{id}")
    Emp findEmpById(Integer id);
}
