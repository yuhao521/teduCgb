package com.jt.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;

@Data
@Accessors(chain = true)
@AllArgsConstructor
@NoArgsConstructor
public class Emp implements Serializable {

    private Integer id;
    private String name;
    private Integer age;
    //一对一 员工在哪个部门
    private Dept dept;
}
