package com.jt.service;

import com.jt.pojo.User;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface UserService {

    //查询所有用户
    List<User> findAllUser();

    //根据id删除用户
    Integer delUserById(Integer id);
}
