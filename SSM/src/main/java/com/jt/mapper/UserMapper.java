package com.jt.mapper;

import com.jt.pojo.User;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface UserMapper {

    //查询所有用户信息
    List<User> findAllUser();

    //根据id删除用户
    Integer delUserById(Integer id);
}
