package com.jt.controller;

import com.jt.pojo.User;
import com.jt.service.UserService;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/user")
@CrossOrigin
public class UserController {

    @Autowired
    private UserService userService;

    //查询所有用户信息
    @GetMapping("/findAll")
    public List<User> findAllUser() {
        List<User> users = userService.findAllUser();
        for (User user : users) {
            System.out.println(user);
        }
        return users;
    }

    //根据id删除用户
    @GetMapping("/deleteUserById/{id}")
    public Integer delUserById(@PathVariable Integer id){
        return userService.delUserById(id);
    }
}
