package com.jt.controller;

import com.jt.pojo.User;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/axios")
@CrossOrigin    //专门解决跨域问题 允许所有的网址访问
//@CrossOrigin(value = "http://www.jt.com")  只允许www.jt.com网址访问
public class AxiosController {

    /**
     * http://localhost:8080/axios/findStr
     */
    @GetMapping("/findStr")
    public String findStr(){

        return "再坚持半天!!!";
    }

    @GetMapping("/getUserById")
    public String findUserById(Integer id){

        return "获取数据:"+id;
    }

    /**
     * 接收对象参数
     * URL: http://localhost:8080/axios/findUser?id=100&name=tomcat
     */
    @GetMapping("/findUser")
    public User findUser(User user){

        return user;
    }

    /**
     * 4.接收restFul的数据
     * URL: http://localhost:8080/axios/result/100/tomcat/19
     * 返回值: User对象
     */
    @GetMapping("/result/{id}/{name}/{age}")
    public User result(User user){

        return user;
    }

    /**
     * 编辑后端Controller
     * URL: /axios/saveUser
     * 参数: json串
     *      {"id":100,"name":"tomcat猫","age":20}
     * 返回值: User
     * 难点:
     *      1.将JSON串转化为Java对象  @RequestBody
     *      2.将Java对象转化为JSON串! @ResponseBody
     */
    @PostMapping("/saveUser")
    public User saveUser(@RequestBody User user){

        return user;
    }
    /**
     * URL: http://localhost:8080/user/restFul2/1/王五/18
     * RestFul对象接收:
     * 如果对象的属性与{key}相同,则可以使用对象接收.
     * 用途: restFul结构 一般用于更新操作
     */
    @GetMapping("/restFul2/{id}/{name}/{age}")
    public User restFul2(User user){

        return user;
    }






}
