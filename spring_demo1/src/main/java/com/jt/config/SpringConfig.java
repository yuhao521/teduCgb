package com.jt.config;

import com.jt.demo2.Dog;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration //标识当前类是配置类
@ComponentScan("包名")//包扫描注解：让spring的注解生效
public class SpringConfig {

    /**
     * @Bean
     * 作用：告诉spring容器，
     * 当前方法的方法名是map中的key
     * 当前方法的返回值是map中的value
     * 特殊用法：
     *  常规条件，spring通过反射实例化对象，也可以由用户自己用new的方式创建
     * @return
     */
    @Bean
    public Dog dog() {
        return new Dog();
    }
}
