package com.jt.config;

import com.jt.demo2.Dog;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class DogConfig {

    @Bean
    public Dog dog() {
        return new Dog();
    }
}
