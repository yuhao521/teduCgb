package com.jt.demo4;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class User {
    public static void main(String[] args) {
        ApplicationContext applicationContext = new AnnotationConfigApplicationContext(SpringConfig.class);
        applicationContext.getBean(Dog.class);
        applicationContext.getBean(Dog.class);
        applicationContext.getBean(Dog.class);
//        dog.hello();
//        dog1.hello();
//        dog2.hello();
    }

}
