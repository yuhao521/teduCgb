package com.jt.demo4;

import org.springframework.context.annotation.*;

@Configuration
@ComponentScan("com.jt.demo4")
public class SpringConfig {

    @Bean
    @Lazy
    @Scope("prototype")
    public Dog dog() {
        return new Dog();
    }
}
