package com.jt.demo3;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class User {

    public static void main(String[] args) {
        //1.创建容器
        ApplicationContext applicationContext = new AnnotationConfigApplicationContext(SpringConfig.class);
        Dog dog = applicationContext.getBean(Dog.class);
        dog.hello();
    }
}
