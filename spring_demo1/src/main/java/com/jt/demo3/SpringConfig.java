package com.jt.demo3;

import org.springframework.context.annotation.*;

@Configuration
@ComponentScan("com.jt.demo3")
public class SpringConfig {

    @Bean
    @Scope("prototype")//多例对象 什么都不写默认是单例 singleton
    @Lazy //开启懒加载
    public Dog dog() {
        return new Dog();
    }
}
