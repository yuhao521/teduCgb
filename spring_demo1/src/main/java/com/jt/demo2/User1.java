package com.jt.demo2;

import com.jt.config.DogConfig;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.ComponentScan;

@ComponentScan("com.jt.config")
public class User1 {

    public static void main(String[] args) {
        //1.创建容器 AnnotationConfigApplicationContext
        ApplicationContext applicationContext = new AnnotationConfigApplicationContext(DogConfig.class);
        Dog dog = applicationContext.getBean(Dog.class);
        dog.hello();
    }
}
