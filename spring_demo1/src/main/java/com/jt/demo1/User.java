package com.jt.demo1;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class User {
    public static void main(String[] args) {
        //1.找到配置文件
        String resource = "spring.xml";
        //2.创建容器 ApplicationContext就是容器 ClassPathXmlApplicationContext 实现类
        ApplicationContext applicationContext = new ClassPathXmlApplicationContext(resource);
        //3.拿到对象
        Dog dog = applicationContext.getBean(Dog.class);
        Dog dog1 = (Dog) applicationContext.getBean("dog");
        //4.调用相关方法
        dog.hello();
        dog1.hello();
    }
}
