package com.jt.service.impl;

import com.jt.mapper.UserMapper;
import com.jt.pojo.User;
import com.jt.service.UserService;
import com.jt.vo.PageResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.DigestUtils;

import java.util.List;
import java.util.UUID;

@Service
public class UserServiceImpl implements UserService {

    private final UserMapper userMapper;

    @Autowired
    public UserServiceImpl(UserMapper userMapper) {
        this.userMapper = userMapper;
    }

    @Override
    public List<User> findAll() {
        return userMapper.findAll();
    }

    /**
     * 用户登录
     * @param user
     * @return
     */
    @Override
    public String findUserByUP(User user) {
        // 1.将密码进行加密
        String password = user.getPassword();
        password = DigestUtils.md5DigestAsHex(password.getBytes());
        // 将秘钥传给用户
        user.setPassword(password);
        //调用mapper中的登陆方法
        User userDB = userMapper.findUserByUP(user);
        System.out.println(userDB);
        // 2. 判断秘钥是否为空
        if (userDB == null) {
            return null;
        }
        // 3.不为空返回秘钥
        //UUID 根据时间秒数哈希
        return UUID.randomUUID().toString();
    }

    /**
     * 查询用户列表
     * @param pageResults
     * @return
     */
    @Override
    public PageResult getUserList(PageResult pageResults) {
        //1.获取数据库中用户的所有记录数
        long total = userMapper.findCount();
        //2.总共多少条记录
        int size = pageResults.getPageSize();
        //3.总共多少页 用页来计算每一页的起始位置
        int start = (pageResults.getPageNum() - 1) * size;
        //4.用户输入的查询条件
        String query = pageResults.getQuery();
        //5.查询用户的分页结果
        List<User> list = userMapper.getUserListResult(query,size,start);
        //6.获取分页的全部结果以及用户的全部记录数 封装并返回
        pageResults.setTotal(total).setRows(list);
        return pageResults;
    }
}
