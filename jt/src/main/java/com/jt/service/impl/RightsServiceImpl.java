package com.jt.service.impl;

import com.jt.mapper.RightsMapper;
import com.jt.pojo.Rights;
import com.jt.service.RightsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RightsServiceImpl implements RightsService {

    @Autowired
    private RightsMapper rightsMapper;

    @Override
    public List<Rights> findAllRights() {
        //查询所有的菜单
        int parentId = 0;
        List<Rights> rights = rightsMapper.findAllRights(parentId);
        /*for (Rights right : rights) {
            //将父级id作为二级菜单的查询条件
            int children = right.getId();
            //将二级id传入
            List<Rights> childrens = rightsMapper.findAllRights(children);
            //将查询出的二级菜单赋值给一级菜单
            right.setChildren(childrens);
        }*/
        return rights;
    }
}
