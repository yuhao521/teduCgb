package com.jt.service;

import com.jt.pojo.ItemCat;
import com.jt.pojo.Rights;
import org.springframework.stereotype.Service;

import java.util.List;

public interface RightsService {

    //查询所有菜单
    List<Rights> findAllRights();
}
