package com.jt.controller;

import com.jt.pojo.Rights;
import com.jt.service.RightsService;
import com.jt.vo.SysResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@CrossOrigin
@RequestMapping("/rights")
public class RightsController {

    private final RightsService rightsService;

    @Autowired
    public RightsController(RightsService rightsService) {
        this.rightsService = rightsService;
    }

    // 1.左侧菜单获取
    @GetMapping("/getRightsList")
    public SysResult getRightsList() {
        //查询所有的菜单
        List<Rights> rights = rightsService.findAllRights();
        return SysResult.success(rights);
    }
}
