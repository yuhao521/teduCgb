package com.jt.mapper;

import com.jt.pojo.Rights;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface RightsMapper {

    //查询所有菜单
    List<Rights> findAllRights(int parentId);
}
